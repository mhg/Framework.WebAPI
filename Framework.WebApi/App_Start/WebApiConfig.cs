﻿using Framework.WebApi.App_Start;
using Swashbuckle.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Framework.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API 配置和服务

            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            InitFormatAPI.Init(config);
            //启用swaggerui
            config.EnableSwagger(c => c.SingleApiVersion("v1", "WebApi开发接口"))
            .EnableSwaggerUi();
        }
    }
}
