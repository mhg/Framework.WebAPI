﻿using Framework.WebApi.App_Start;
using Swashbuckle.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]
namespace Framework.WebApi.App_Start
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "WebApi");
                    c.IncludeXmlComments(string.Format("{0}/bin/CanYinItem.Api.xml", System.AppDomain.CurrentDomain.BaseDirectory));
                    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                })
                .EnableSwaggerUi(c =>
                {
                    c.DocumentTitle("WebAPI系统开发接口");

                    //c.InjectJavaScript(thisAssembly, "XXX.XXX.WebApi.Scripts.Swagger.swagger_lang.js");

                });
        }

    }
}