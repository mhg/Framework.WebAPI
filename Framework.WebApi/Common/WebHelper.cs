﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Framework.WebApi.Common
{
    public class WebHelper
    {
        public const string keyStr = "Framework.WebApi-mhg-8888";//切莫私自更改，服务端和前台保持一致
        /// <summary>
        /// 生成验证用的Token
        /// </summary>
        /// <param name="loginTime">The login time.</param>
        /// <returns></returns>
        public static string GetTokenString(string loginTime)
        {
            string tempStr = keyStr + loginTime;

            string md5Str = MD5Encrypt(tempStr);

            return md5Str;
        }

        /// <summary>  
        /// 获取时间戳  
        /// </summary>  
        /// <returns></returns>  
        public static string GetTimeStamp()
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)); // 当地时区
            long timeStamp = (long)(DateTime.Now - startTime).TotalMilliseconds; // 相差毫秒数
            return timeStamp.ToString();
        }
        ///   <summary>
        ///   MD5加密
        ///   </summary>
        ///   <param   name="strText">待加密字符串</param>
        ///   <returns>加密后的字符串</returns>
        private static string MD5Encrypt(string strText)
        {
            string tempStr = string.Empty;
            using (MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(strText));
                StringBuilder sBuilder = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
                tempStr = sBuilder.ToString();
            }
            return tempStr;
        }
    }
}