﻿using Framework.WebApi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Framework.WebApi.Filter
{
    public class MyAuthFilterAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            try
            {
                //如果用户方位的Action带有AllowAnonymousAttribute，则不进行授权验证
                if (actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any())
                {
                    return;
                }
                string authParameter = null;
                var authValue = actionContext.Request.Headers.Authorization;
                if (authValue != null && authValue.Scheme == "BasicAuth")
                {
                    authParameter = authValue.Parameter;  //获取请求参数
                    var authToken = authParameter.Split('|');  //取出参数,参数格式为（当前时间：加密后的token）将其进行分割           
                                                               //LogHelper.WriteLog("\r\n authParameter:" + authParameter);
                    if (authToken.Length < 2)
                    {
                        actionContext.Response = new HttpResponseMessage(HttpStatusCode.NotAcceptable);//参数不完整，返回406不接受
                    }
                    else
                    {
                        //参数完整，进行验证
                        if (ValidateToken(authToken[0], authToken[1]))
                        {
                            base.OnAuthorization(actionContext);
                        }
                        else
                        {
                            actionContext.Response = new HttpResponseMessage(HttpStatusCode.ExpectationFailed);//验证不通过，未满足期望值417
                        }
                    }
                }
                else
                {
                    string currentUrl = actionContext.Request.RequestUri.ToString();
                  //  LogHelper.WriteLog($"\r\n{DateTime.Now.ToString("yyyy-mm-dd:MM:HH:ss")}：当前请求为：" + currentUrl + " |" + "Token不正确", "API接口检验");
                    //如果验证不通过，则返回401错误，并且Body中写入错误原因
                    actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, new HttpError("Token不正确"));
                }
            }
            catch (Exception e)
            {
               // LogHelper.WriteLog($"\r\n{e.Message}", "异常信息");
                throw;
            }

        }
        //验证token
        public bool ValidateToken(string loginTime, string token)
        {
            bool flag = true;
            if (string.IsNullOrEmpty(loginTime))
            {
                flag = false;
            }
            DateTime checkTime = DateTime.Parse(loginTime);
            //验证时间是否过期
            DateTime nowtime = DateTime.Now;
            TimeSpan a = nowtime - checkTime;
            if (a.TotalSeconds > 120)//时间过期
            {
                flag = false;
            }
            else
            {
                string checkToken = WebHelper.GetTokenString(loginTime);
                //LogHelper.WriteLog("\r\n1:" + checkToken + ";2:" + token);
                //比较token
                if (token.Equals(checkToken, StringComparison.CurrentCultureIgnoreCase))
                {
                    flag = true;
                }
                else
                {
                    flag = false;
                }
            }
            return flag;
        }
    }
}