﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Framework.WebApi.Models
{
    public class Example
    {
        public string  UserName { get; set; }
        public DateTime CreateTime { get; set; }
        public string  Address { get; set; }
    }
}