﻿using Framework.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Framework.WebApi.Controllers
{
    public class ValueController : ApiController
    {
        // GET: api/Value
        public IHttpActionResult Get1()
        {
            Example model = new Example() {
                UserName= "码农也有梦想",
                CreateTime=DateTime.Now,
                Address=null
            };
            return Ok(model);
        }
        public IHttpActionResult Get2()
        {
            Example model = new Example()
            {
                UserName = "码农也有梦想",
                CreateTime = DateTime.Now,
                Address = null
            };
            return Json(model);
        }
        // GET: api/Value/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Value
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Value/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Value/5
        public void Delete(int id)
        {
        }
    }
}
